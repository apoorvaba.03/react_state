import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Toggle from './Toggle';
import Greeting from './Greeting';
import reportWebVitals from './reportWebVitals';
class Clock extends React.Component {
  constructor(props){
    super(props);
    this.state = {date:new Date()};
  }
  componentDidMount(){
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }
  componentWillUnmount(){
    clearInterval(this.timerID);
  }
  tick(){
    this.setState({
      date:new Date()
    });
  }
  
  render() {
    return (
      <div>
        <h1>Hello wolrlld</h1>
        
        <h2>
          It is {this.state.date.toLocaleTimeString()}.
        </h2>
      </div>
    );
  }
}

//function tick(){
ReactDOM.render(
  /*<Clock date={new Date()} />,*/
 // <Clock />,
 // <Toggle />,
 //Try changing to isLoggedIn = {false}
 <Greeting isLoggedIn={true}/>,
  document.getElementById('root')
  
);

//setInterval(tick,1000);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
